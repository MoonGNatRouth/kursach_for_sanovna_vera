import by.sanovna.vera.dao.AccountDao;
import by.sanovna.vera.dao.MovieDao;
import by.sanovna.vera.dao.ReservationDao;
import by.sanovna.vera.dao.SessionDao;
import by.sanovna.vera.model.Account;
import by.sanovna.vera.model.AccountRole;
import by.sanovna.vera.model.Movie;
import by.sanovna.vera.model.Reservation;
import by.sanovna.vera.model.ReservationPlace;
import by.sanovna.vera.model.Session;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@WebAppConfiguration
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/config/data.xml"})
public class RepositoryTest {
    Logger logger = Logger.getLogger(RepositoryTest.class);

    @Autowired
    private MovieDao movieDao;

    @Autowired
    private SessionDao sessionDao;

    @Autowired
    private ReservationDao reservationDao;

    @Autowired
    private AccountDao accountDao;

    @Test
    public void getAll() {
        List<Movie> movieList = movieDao.getAll();
        logger.info("size="+movieList.size());
        for (Movie movie : movieList) {
            logger.info(movie.toString());
        }
    }

    @Test
    public void movieSimpleCRUD() {
        int initSize = movieDao.getAll().size();

        Movie movie = new Movie();
        movie.setName("Имя");
        movie.setGenre("genre");
        movie.setDuration(14);
        movieDao.saveOrUpdate(movie);
        Assert.assertTrue(movie.getId()!=0);

        int newSize = movieDao.getAll().size();
        Assert.assertEquals(initSize+1, newSize);

        Movie movie1 = movieDao.getById(movie.getId());
        Assert.assertNotNull(movie1);

        movie1.setDuration(99);
        movieDao.saveOrUpdate(movie1);
        movie1 = movieDao.getById(movie1.getId());
        Assert.assertTrue(movie1.getDuration()==99);

        movieDao.deleteById(movie1.getId());
        movie1 = movieDao.getById(movie1.getId());
        Assert.assertNull(movie1);
    }

    @Test
    public void movieInsertedAndSessionCRUD() {
        Movie movie = new Movie();
        movie.setName("TestName11");
        movie.setGenre("TestGenre11");
        movie.setDuration(123);
        movie.setPosterFileExt("jpg");

        List<Session> sessionList = new LinkedList<Session>();
        Session session1 = new Session();
        session1.setDate(new Date());
        session1.setCost(34.0f);
        sessionList.add(session1);
        Session session2 = new Session();
        session2.setDate(new Date());
        session2.setCost(23.0f);
        sessionList.add(session2);

        movie.setSessions(sessionList);

        movieDao.saveOrUpdate(movie);
        Assert.assertFalse(movie.getId()==0);
        Assert.assertFalse(session1.getId()==0);
        Assert.assertFalse(session2.getId()==0);

        movie = movieDao.getById(movie.getId());
        Assert.assertNotNull(movie);
        Assert.assertNotNull(movie.getSessions());
        Assert.assertEquals(movie.getSessions().size(), 2);
        for (Session session : movie.getSessions())
            Assert.assertTrue(session.getId()==session1.getId() || session.getId()==session2.getId());

        session1.setCost(10);
        sessionDao.saveOrUpdate(session1);
        Session session3 = sessionDao.getById(session1.getId());
        Assert.assertNotNull(session3);
        Assert.assertEquals(session1.getCost(),session3.getCost());

        movieDao.deleteById(movie.getId());
        Assert.assertNull(movieDao.getById(movie.getId()));
        Assert.assertNull(sessionDao.getById(session1.getId()));
        Assert.assertNull(sessionDao.getById(session2.getId()));
    }

    @Test
    public void reservationRepositoryTest() {
        //create movie & session
        Session session = new Session();
        session.setCost(23);
        session.setDate(new Date());
        Movie movie = new Movie();
        movie.setSessions(Collections.singletonList(session));
        movie.setDuration(123);
        movie.setGenre("testGenre");
        movie.setName("testName");
        movieDao.saveOrUpdate(movie);

        //create & test account
        Account account = new Account();
        account.setName("testUser");
        account.setPassword("123");
        account.setRole(AccountRole.ROLE_USER);
        account.setEmail("test@user.by");
        accountDao.save(account);
        Assert.assertFalse(account.getId()==0);
        Assert.assertNotNull(accountDao.getById(account.getId()));
        Assert.assertNotNull(accountDao.getByEmail(account.getEmail()));

        //create & test reservation
        Reservation reservation = new Reservation();
        reservation.setAccount(account);
        reservation.setSession(session);
        reservation.setPlaces(Collections.singletonList(new ReservationPlace(10, 10)));
        reservationDao.save(reservation);
        Assert.assertFalse(reservation.getId() == 0);
        Assert.assertNotNull(reservationDao.getById(reservation.getId()));

        List<Reservation> reservations = reservationDao.getByAccountId(account.getId());
        boolean contains = false;
        for (Reservation accountReservation : reservations) {
            if (accountReservation.getId()==reservation.getId())
                contains=true;
        }
        Assert.assertTrue(contains);

        reservations = reservationDao.getBySessionId(session.getId());
        contains = false;
        for (Reservation sessionReservation : reservations) {
            if (sessionReservation.getId()==reservation.getId())
                contains=true;
        }
        Assert.assertTrue(contains);

        Assert.assertFalse(reservation.isSold());
        reservationDao.sellReservation(reservation.getId());
        reservation = reservationDao.getById(reservation.getId());
        Assert.assertTrue(reservation.isSold());

        reservationDao.deleteById(reservation.getId());
        Assert.assertNull(reservationDao.getById(reservation.getId()));

        //we can't test this method, cause we can damage DB data
        //reservationDao.deleteAllReservationsForNMinutesBeforeSession();
    }

}
