import by.sanovna.vera.dao.DbPool;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class DbPoolTest {

    static Logger logger = Logger.getLogger(DbPoolTest.class);

    static DbPool dbPool;

    @BeforeClass
    public static void loadClass() throws ClassNotFoundException, SQLException {
        dbPool = new DbPool("com.mysql.jdbc.Driver","jdbc:mysql://localhost/cinema","vera","vera",20);
    }

    @Test
    public void testSomething() throws SQLException {
        logger.info("test1");

        ExecutorService es = Executors.newFixedThreadPool(5);
        for (int i=0; i<5; i++)
            es.submit(new Runnable() {
                @Override
                public void run() {
                    Connection connection = null;
                    try {
                        connection = dbPool.getConnection();
                        testConnection(connection);
                    } catch (SQLException e) {
                        e.printStackTrace();
                        Assert.assertTrue(false);
                    } finally {
                        dbPool.putConnection(connection);
                    }
                }
            });
        try {
            Thread.sleep(1000);
            es.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private void testConnection(Connection connection) throws SQLException {
        logger.info("connected");
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("select id from movie");
        while (rs.next()) {
            logger.info("id="+rs.getInt("id"));
        }

        rs.close();
        statement.close();
//        connection.close();
        logger.info("disconnected");
    }

    @Test
    public void testSomethingElse() throws SQLException, InterruptedException {
        logger.info("test2");
        Connection connection = dbPool.getConnection();
        testConnection(connection);
        dbPool.putConnection(connection);

    }

}
