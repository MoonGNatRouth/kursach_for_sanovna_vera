<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><tiles:insertAttribute name="title" ignore="true" /></title>

    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap/bootstrap.css"/>" />
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap/bootstrap-theme.css"/>" />
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/layout.css"/>" />
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/menu.css"/>" />
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main.css"/>" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="<c:url value='/resources/js/bootstrap.js' />"></script>
    <script src="<c:url value='/resources/js/dynamic_list_helper.js' />"></script>
</head>
<body>
<div class="header row">
    <tiles:insertAttribute name="header" />
</div>
<div class="container content col-lg-offset-0" >
    <div class="row">

        <div class="menu col-lg-3">
            <tiles:insertAttribute name="menu" />
        </div>
        <div class="col-lg-9" >
            <tiles:insertAttribute name="body" />
        </div>
    </div>
</div>
<hr>
<div class="container col-lg-12">
    <tiles:insertAttribute name="footer" />
</div>
</body>
</html>