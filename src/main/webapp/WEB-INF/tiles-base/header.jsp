<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spr"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation" >
    <ul class="nav navbar-nav">
        <li><a href="<c:url value='/'/>"><spr:message code='home'/></a></li>
        <li><a href="<c:url value='/movies'/>"><spr:message code='movies'/></a></li>
        <sec:authorize access="isFullyAuthenticated()">
                <li><a href="<c:url value='/reservations'/>"><spr:message code='reservations'/></a></li>
        </sec:authorize>
    </ul>
</div>