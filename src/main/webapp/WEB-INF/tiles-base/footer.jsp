<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spr"%>
<footer>
    <p>&copy; <spr:message code='all_rights_reserved'/></p>
</footer>