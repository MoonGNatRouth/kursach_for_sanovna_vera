<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<ul class="nav nav-pills nav-stacked">
    <sec:authorize access="isAnonymous()">
        <li><a href="<c:url value='/login'/>"><spr:message code='login'/></a></li>
        <li><a href="<c:url value='/registration'/>"><spr:message code='registration'/></a></li>
    </sec:authorize>
    <sec:authorize access="isFullyAuthenticated()">
            <li><a href="<c:url value='/j_spring_security_logout'/>"><spr:message code='logout'/></a></li>
    </sec:authorize>
    <hr>
    <li><a href='?lang=en'>English</a></li>
    <li><a href='?lang=ru'>Русский</a></li>
</ul>