<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spr"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<style type='text/css'>
    .rowNum {
        width: 40px;
        height : 40px;
    }
    .sit {
        width: 40px;
        height : 40px;
        border : 5px solid #777777;
    }
    .free {
        background-color : #999999;
    }
    .reserved {
        background-color : #9999CC;
    }
    .sold {
        background-color : #CC9999;
    }
    .screen {
        background-color : #9999CC;
        height : 60px;
    }
    .screenSpacer {
        height:30px;
    }
    .selected {
        background-color : #CCCCCC;
     }
</style>

<h3><spr:message code='reservation_for_movie'/>: "${movie.name}"</h3>
<hr />
<table width='100%'><tr>
    <td width='99%'>
        <spr:message code='date'/>: <fmt:formatDate value="${session.date}" pattern="HH:mm dd.MM.yyyy" /><br/>
        <spr:message code='cost'/>: <span id="totalCost"></span><br/>
        <spr:message code='sits'/>: <span id="sits"></span>
    </td>
    <td width=50>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <input type='button' value="<spr:message code='release'/>" id='releaseButton'/>
            <input type='button' value="<spr:message code='sell'/>" id='sellButton'/>
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_USER')">
            <input type='button' value="<spr:message code='submit'/>" id='submitButton'/>
        </sec:authorize>
    </td>
</tr></table>
<hr />


<c:if test="${not empty matrix}">
    <table>
        <tr>
            <td colspan="${fn:length(matrix[0]) + 2}" align='center' class="screen"><spr:message code='screen'/></td>
        </tr>
        <tr>
            <td class='screenSpacer'>&nbsp;</td>
        </tr>
        <c:forEach items="${matrix}" var="row" varStatus="rowStatus">
            <c:set var="rowNum" value="${rowStatus.index+1}" />
            <tr valign="center">
                <td class='rowNum' align='left'>${rowNum}</td>
                <c:forEach items="${row}" var="item" varStatus="itemStatus">
                    <c:set var="itemNum" value="${itemStatus.index+1}" />

                    <c:if test="${item == null}" var="sitClass">
                        <c:set var="sitClass" value="free" />
                    </c:if>
                    <c:if test="${item != null && item.sold == false}">
                        <c:set var="sitClass" value="reserved res${item.id}" />
                    </c:if>
                    <c:if test="${item != null && item.sold == true}">
                        <c:set var="sitClass" value="sold res${item.id}" />
                    </c:if>

                    <!-- cell -->
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <td id='sit${rowNum}p${itemNum}' class='sit ${sitClass}' align='center'  onclick='selectSits(${rowNum},${itemNum})'>
                            ${itemNum}
                            <c:if test="${item != null}">
                                <br/>${item.account.name}
                            </c:if>
                        </td>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <td id='sit${rowNum}p${itemNum}' class='sit ${sitClass}' align='center'  onclick='selectSits(${rowNum},${itemNum})'>
                            ${itemNum}
                        </td>
                    </sec:authorize>

                </c:forEach>
                <td class='rowNum' align='right'>${rowNum}</td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<c:if test="${empty matrix}">
    <spr:message code='its_a_mistake'/>
</c:if>



<sec:authorize access="hasRole('ROLE_USER')">
    <script type='text/javascript'>
        var selectedSits = [];

        function selectSits(row, place) {
            var elt = $('#sit'+row+'p'+place);
            if (elt.hasClass('reserved') || elt.hasClass("sold"))
                    return;

            var sit = [row, place];
            var inIt = false;
            selectedSits.forEach( function(element, index, array) {
                if (element[0]==sit[0] && element[1]==sit[1]) {
                    array.splice(index,1);
                    inIt = true;
                }
            });
            if (!inIt) {
                if (selectedSits.length>=7)
                    return;
                selectedSits.push(sit);
                elt.addClass('selected');
            } else {
                elt.removeClass('selected');
            }

            $('#sits').html(selectedSits.join(' ; '));
            $('#totalCost').html(selectedSits.length*${session.cost});
        }

        $('#submitButton').click( function() {
            if (selectedSits.length<1) {
                alert("<spr:message code='select_anything'/>");
                return;
            }
            if (selectedSits.length>7) {
                alert('too many chiter!');
                return;
            }

            var newLocation = "<c:url value='/session/reserve'/>?session=${session.id}&";
            selectedSits.forEach( function(element, index, array) {
                newLocation += "row="+element[0]+"&place="+element[1]+"&";
            });
            window.location = newLocation;
        });
    </script>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_ADMIN')">
<script type='text/javascript'>
        var selectedSits = [];

        function selectSits(row, place) {
            var curSelectedSits = []
            var elt = $('#sit'+row+'p'+place);
            if (elt.hasClass('reserved') || elt.hasClass('sold')) {
                var className = getSameReservedClassName(elt);
                console.info("select all with this class="+className);
                $("."+className).each(function() {
                    console.info($(this).attr('id'));
                    var newSit = parseRowAndPlace($(this).attr('id'));
                    curSelectedSits.push(newSit);
                });
            } else {
                curSelectedSits.push([row, place]);
            }
            console.info(curSelectedSits);

            if (isSelected(row,place)) {
                for (var i=0; i<curSelectedSits.length; i++) {
                    deleteFromSelected(curSelectedSits[i][0], curSelectedSits[i][1]);
                    $('#sit'+curSelectedSits[i][0]+'p'+curSelectedSits[i][1]).removeClass('selected');
                }
            } else {
                for (var i=0; i<curSelectedSits.length; i++) {
                    selectedSits.push(curSelectedSits[i]);
                    $('#sit'+curSelectedSits[i][0]+'p'+curSelectedSits[i][1]).addClass('selected');
                }
            }

            $('#sits').html(selectedSits.join(' ; '));
        }

        function deleteFromSelected(row, place) {
            selectedSits.forEach( function(element, index, array) {
                if (element[0]==row && element[1]==place) {
                    array.splice(index,1);
                }
            });
        }

        function isSelected(row, place) {
            for (var i=0; i<selectedSits.length; i++)
                if (selectedSits[i][0]==row && selectedSits[i][1]==place) {
                    return true;
                }
            return false;
        }

        function parseRowAndPlace(id) {
            strArray = id.substring(3).split('p');
            return [ parseInt(strArray[0]), parseInt(strArray[1]) ]
        }

        function getSameReservedClassName(elt) {
            var clazzes = elt.attr('class').split(/\s+/);
            for (var i=0; i<clazzes.length; i++) {
                if (clazzes[i].match(/res\d+/)) {
                    return clazzes[i];
                }
            }
        }

        $('#releaseButton').click( function() {
            if (selectedSits.length<1) {
                alert("<spr:message code='select_anything'/>");
                return;
            }

            var newLocation = "<c:url value='/session/release'/>?session=${session.id}&";
            selectedSits.forEach( function(element, index, array) {
                newLocation += "row="+element[0]+"&place="+element[1]+"&";
            });
            window.location = newLocation;
        });

        $('#sellButton').click( function() {
            if (selectedSits.length<1) {
                alert("<spr:message code='select_anything'/>");
                return;
            }

            var newLocation = "<c:url value='/session/sell'/>?session=${session.id}&";
            selectedSits.forEach( function(element, index, array) {
                newLocation += "row="+element[0]+"&place="+element[1]+"&";
            });
            window.location = newLocation;
        });
    </script>
</sec:authorize>