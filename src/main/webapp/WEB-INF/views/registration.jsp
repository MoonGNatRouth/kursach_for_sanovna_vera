<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spr"%>

<div class="form-group col-lg-4">
    <h3><spr:message code='registration'/></h3>

    <form:form action="registration" commandName="account" modelAttribute="account" method="POST">

        <form:input type="email" path="email" placeholder="email" class="form-control"/> <form:errors path="email"/> <br/>
        <form:input path="name" placeholder="First Name" class="form-control"/> <form:errors path="name"/> <br/>
        <form:input type="password" path="password" placeholder="password" class="form-control"/> <form:errors path="password"/> <br/>

        <input type="submit" value="<spr:message code='registration'/>" class="btn btn-success"/>
    </form:form>

</div>