<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spr"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value='/movie/createOrUpdate' var="actionUrl"/>
<!-- enctype="multipart/form-data"  -->
<form:form action="" id='movieCreateForm' commandName="Movie" modelAttribute="movie" method="POST"  enctype="multipart/form-data"  accept-charset="UTF-8">

    <form:input path="id" type="hidden"/>

    <form:label path="name"><spr:message code='movie_name'/></form:label>
    <form:input path="name" cssClass="form-control"/> <form:errors path="name"/> <br/>

    <form:label path="genre"><spr:message code='genre'/></form:label>
    <form:input path="genre" cssClass="form-control" /><form:errors path="genre"/><br />

    <form:label path="duration"><spr:message code='duration'/></form:label>
    <form:input path="duration" cssClass="form-control" /><form:errors path="duration"/><br />

    <b><spr:message code='poster'/>:</b> <input type="file" name="poster"><br/>

    <hr/>
    <b><center><spr:message code='sessions'/></center></b>
    <table>
        <thead>
            <th><spr:message code='date'/></th>
            <th><spr:message code='cost'/></th>
            <th>&nbsp;</th>
        </thead>
        <tbody id="sessionContainer">
            <c:forEach items="${movie.sessions}" var="session" varStatus="status">
                <tr class="session">
                        <form:input type='hidden' class='id' path="sessions[${status.index}].id" />
                    <td>
                        <form:input type='datetime'  class='date' placeholder="HH:mm dd.MM.yyyy" path="sessions[${status.index}].date" />
                        <form:errors path="sessions[${status.index}].date"/>
                    </td>
                    <td>
                        <form:input class='cost' placeholder="<spr:message code='cost'/>" path="sessions[${status.index}].cost" />
                        <form:errors path="sessions[${status.index}].cost"/>
                    </td>
                    <td>
                        <a href='#' class='removeSession'><spr:message code='remove'/></a>
                    </td>
                </tr>
            </c:forEach>
            <tr class="session defaultRow">
                <input type="hidden" class='id' name="sessions[].id" value="0" />
                <td><input type="datetime" class='date' placeholder="HH:mm dd.MM.yyyy" name="sessions[].date" value="" /></td>
                <td><input type="text" class='cost' name="sessions[].cost" value="" /></td>
                <td><a href="#" class="removeSession"><spr:message code='remove'/></a></td>
            </tr>
        </tbody>
    </table>
    <a href="#" id="addSession"><spr:message code='add_session'/></a>
    <hr/>
    <input type="submit" value="<spr:message code='submit'/>" class="btn btn-default form-control" />
</form:form>


<script type="text/javascript">
    //js for adding session rows
    function rowAdded(rowElement) {
        $(rowElement).find("input").val('');
        $(rowElement).find("input.cost").val('0');
        $(rowElement).find("input.id").val('0');
    }
    function rowRemoved(rowElement) {
    }

    function beforeSubmit() {
        return true;
    }

    $(document).ready( function() {
        var config = {
            rowClass : 'session',
            addRowId : 'addSession',
            removeRowClass : 'removeSession',
            formId : 'movieCreateForm',
            rowContainerId : 'sessionContainer',
            indexedPropertyName : 'sessions',
            indexedPropertyMemberNames : 'id,date,cost',
            rowAddedListener : rowAdded,
            rowRemovedListener : rowRemoved,
            beforeSubmit : beforeSubmit
        };
        new DynamicListHelper(config);
    });
</script>