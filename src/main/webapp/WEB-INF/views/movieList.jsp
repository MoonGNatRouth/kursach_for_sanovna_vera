<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spr"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<h3><spr:message code='movies'/></h3>
<hr/>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <a href="<c:url value='/movie/create'/>"><spr:message code='create_movie'/></a><br>
</sec:authorize>

<c:if test="${not empty movies}">
    <c:forEach items="${movies}" var="movie">
        <table>
            <tr>
                <td>
                    <c:if test="${empty movie.posterFileExt}">
                        <img src="<c:url value='/resources/img/NoImage.jpg'/>" class='poster'/>
                    </c:if>
                    <c:if test="${not empty movie.posterFileExt}">
                        <img src="<c:url value='/resources/img/${movie.id}.${movie.posterFileExt}'/>" class='poster'/>
                    </c:if>

                </td>
                <td>
                    <spr:message code='movie_name'/>: <b>${movie.name}</b><br/>
                    <spr:message code='genre'/>: ${movie.genre}<br/>
                    <spr:message code='duration'/>: ${movie.duration}<br/>
                    <a href="<c:url value='/movie/${movie.id}' />"><spr:message code='description'/></a><br/>

                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <a href="<c:url value='/movie/${movie.id}/edit' />"><spr:message code='edit'/></a><br/>
                        <a href="<c:url value='/movie/${movie.id}/delete' />"><spr:message code='delete'/></a>
                    </sec:authorize>
                </td>
            </tr>
        </table>
        <hr>
    </c:forEach>
</c:if>
<c:if test="${empty movies}">
    <spr:message code='there_are_no_movies'/> =(
</c:if>
