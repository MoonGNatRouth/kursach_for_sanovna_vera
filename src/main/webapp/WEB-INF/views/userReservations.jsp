<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spr"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<h3><spr:message code='reservations' /></h3>
<hr>

<c:if test="${not empty reservations}">
<table border=1>
    <thead>
        <th><spr:message code='movie_name' /></th>
        <th><spr:message code='date'/></th>
        <th><spr:message code='sits'/> (<spr:message code='row'/>, <spr:message code='place'/>)</th>
        <th>&nbsp;</th>
    </thead>
    <tbody>
        <c:forEach items="${reservations}" var="item">
            <tr>
                <td>
                    <a href="<c:url value='/movie/${item.session.movie.id}'/>"/>
                        ${item.session.movie.name}
                    </a>
                </td>
                <td>
                    <a href="<c:url value='/session/${item.session.id}/reserve'/>"/>
                        <fmt:formatDate value="${item.session.date}" pattern="HH:mm dd.MM.yyyy" />
                    </a>
                </td>
                <td>
                    <c:forEach items="${item.places}" var="place">
                        (${place.row}, ${place.place});
                    </c:forEach>
                </td>
                <td>
                    <a href="<c:url value='/reservation/user/release?reservation=${item.id}'/>"/>
                        <spr:message code='release' />
                    </a>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>
</c:if>
<c:if test="${empty reservations}">
    <b><spr:message code='you_have_no_reservations' /></b>
</c:if>