<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spr"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<h4><spr:message code='about_movie'/> "${movie.name}"</h4>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <a href="<c:url value='/movie/${movie.id}/edit'/>"><spr:message code='edit'/></a><br/>
    <a href="<c:url value='/movie/${movie.id}/delete'/>"><spr:message code='delete'/></a>
</sec:authorize>
<hr />
<table>
    <tr valign='top'>
        <td>
            <c:if test="${empty movie.posterFileExt}">
                <img src="<c:url value='/resources/img/NoImage.jpg'/>" class='poster'/>
            </c:if>
            <c:if test="${not empty movie.posterFileExt}">
                <img src="<c:url value='/resources/img/${movie.id}.${movie.posterFileExt}'/>" class='poster'/>
            </c:if>
        </td>
        <td>
            <spr:message code='movie_name'/>: <b>${movie.name}</b><br/>
            <spr:message code='genre'/>: ${movie.genre}<br/>
            <spr:message code='duration'/>: ${movie.duration}<br/>
        </td>
    </tr>

</table>
<hr>

<c:if test="${not empty movie.sessions}">
<b><spr:message code='sessions'/></b>
    <table border=1 width=100%>
        <thead>
            <th><spr:message code='date'/></th>
            <th><spr:message code='cost'/></th>
            <th>&nbsp;</th>
        </thead>
        <tbody>
            <c:forEach items="${movie.sessions}" var="session">
                <tr>
                    <td><fmt:formatDate value="${session.date}" pattern="HH:mm dd.MM.yyyy" /></td>
                    <td>${session.cost}</td>
                    <td><a href="<c:url value='/session/${session.id}/reserve'/>"><spr:message code='reserve'/></a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</c:if>
<c:if test="${empty movie.sessions}">
    <b><spr:message code='there_are_no_sessions'/> =(</b>
</c:if>