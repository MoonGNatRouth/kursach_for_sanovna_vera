package by.sanovna.vera.controller;

import by.sanovna.vera.controller.exceptions.ResourceNotFoundException;
import by.sanovna.vera.model.Movie;
import by.sanovna.vera.service.MovieService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.validation.Valid;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminMovieController {
    private static final Logger logger = Logger.getLogger(AdminMovieController.class);

    @Autowired
    private MovieService movieService;

    @Autowired
    private ServletContext servletContext;

    @RequestMapping(value = {"/movie/create"}, method = {RequestMethod.GET})
    public ModelAndView movieCreatePage() {
        logger.info("create movie page");
        ModelAndView mav = new ModelAndView("movieCreate");
        Movie newMovie = new Movie();
        mav.addObject("movie",newMovie);
        return mav;
    }

    @RequestMapping(value = {"/movie/create"}, method = {RequestMethod.POST})
    public String movieCreateHandler(@RequestParam("poster") MultipartFile poster,
                                         @Valid Movie movie,
                                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "movieCreate";
        } else {
            movieService.createOrUpdateMovie(movie, poster);
            return "redirect:/movies";
        }
    }

    @RequestMapping(value = {"/movie/{id}/edit"}, method = {RequestMethod.GET})
    public ModelAndView movieEditPage(@PathVariable(value = "id") int movieId) {
        Movie movie = movieService.getById(movieId);
        if (movie==null) {
            throw (new ResourceNotFoundException());
        }

        logger.info("edit movie " + movie);
        ModelAndView mav = new ModelAndView("movieCreate");
        mav.addObject("movie", movie);
        return mav;
    }

    @RequestMapping(value = {"/movie/{id}/edit"}, method = {RequestMethod.POST})
    public String movieEditHandler(@RequestParam("poster") MultipartFile poster,
                                         @Valid Movie movie,
                                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "movieCreate";
        } else {
            movieService.createOrUpdateMovie(movie, poster);
            return "redirect:/movies";
        }
    }


    @RequestMapping(value = "/movie/{id}/delete")
    public String deleteMovie(@PathVariable(value = "id") int movieId) {
        logger.info("deleting movie "+movieId);
        movieService.deleteMovie(movieId);
        return "redirect:/movies";
    }
}
