package by.sanovna.vera.controller;

import by.sanovna.vera.controller.exceptions.BadHttpRequest;
import by.sanovna.vera.controller.exceptions.ResourceNotFoundException;
import by.sanovna.vera.model.Account;
import by.sanovna.vera.model.Movie;
import by.sanovna.vera.model.Reservation;
import by.sanovna.vera.model.ReservationPlace;
import by.sanovna.vera.model.Session;
import by.sanovna.vera.service.AccountService;
import by.sanovna.vera.service.MovieService;
import by.sanovna.vera.service.ReservationService;
import by.sanovna.vera.service.SessionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@PreAuthorize("isFullyAuthenticated()")
public class ReservationController {
    private static final Logger logger = Logger.getLogger(ReservationController.class);

    @Autowired
    private ReservationService reservationService;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private MovieService movieService;
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/session/{id}/reserve")
    public ModelAndView reservationPage(@PathVariable(value = "id") int sessionId) {
        Session session = sessionService.getById(sessionId);
        logger.info("session="+session);
        if (session==null)
            throw (new ResourceNotFoundException());
        Movie movie = movieService.getById(session.getMovie().getId());

        List<List<Reservation>> matrix = reservationService.getReservationMatrix(sessionId);

        ModelAndView mav = new ModelAndView("reservation");
        mav.addObject("matrix", matrix);
        mav.addObject("session", session);
        mav.addObject("movie", movie);
        return mav;
    }

    @RequestMapping(value = "/session/reserve")
    public ModelAndView reservation(Authentication authentication,
                                                @RequestParam("session") int sessionId,
                                               @RequestParam("row") List<Integer> rows,
                                               @RequestParam("place") List<Integer> places ) {
        logger.info("here! rows="+rows+" places="+places);
        if (rows.size()!=places.size())
            throw (new BadHttpRequest());
        Session session = sessionService.getById(sessionId);
        logger.info("session="+session);
        if (session==null)
            throw (new BadHttpRequest());
        String userEmail = ((User) authentication.getPrincipal()).getUsername();

        List<ReservationPlace> placeList = new ArrayList<ReservationPlace>(rows.size());
        for (int i=0; i<rows.size(); i++) {
            placeList.add( new ReservationPlace(rows.get(i)-1, places.get(i)-1) );
        }

        reservationService.reserve(userEmail, session, placeList);
        return new ModelAndView("redirect:/session/"+sessionId+"/reserve");
    }

    @RequestMapping(value = "/reservations")
    public ModelAndView userReservations(Authentication authentication) {
        String userEmail = ((User) authentication.getPrincipal()).getUsername();
        Account account  = accountService.getAccountByEmail(userEmail);
        List<Reservation> reservations = reservationService.getByAccountId(account.getId());
        ModelAndView mav = new ModelAndView("userReservations");
        mav.addObject("reservations", reservations);
        return mav;
    }

    @RequestMapping(value = "/reservation/user/release")
    public ModelAndView reservationRelease(@RequestParam("reservation") int reservationId,
                                           Authentication authentication) {
        String userEmail = ((User) authentication.getPrincipal()).getUsername();
        Reservation reservation = reservationService.getById(reservationId);
        if (!reservation.getAccount().getEmail().equals(userEmail)) {
            throw (new AccessDeniedException("You have no permissions to release someone else's reservation"));
        }

        reservationService.release(reservationId);
        return new ModelAndView("redirect:/reservations");
    }
}
