package by.sanovna.vera.controller;

import by.sanovna.vera.dao.DaoException;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController {
    private static final Logger logger = Logger.getLogger(ExceptionHandlerController.class);

    @ExceptionHandler(DaoException.class)
    public String exceptionHandler(DaoException ex) {
        logger.error(ex.toString(),ex);
        ex.printStackTrace();
        logger.error("check connection to Database");
        return "exception";
    }
}
