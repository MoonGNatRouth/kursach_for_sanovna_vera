package by.sanovna.vera.controller;

import by.sanovna.vera.controller.exceptions.BadHttpRequest;
import by.sanovna.vera.model.ReservationPlace;
import by.sanovna.vera.model.Session;
import by.sanovna.vera.service.MovieService;
import by.sanovna.vera.service.ReservationService;
import by.sanovna.vera.service.SessionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminReservationController {
    private static final Logger logger = Logger.getLogger(AdminReservationController.class);

    @Autowired
    private ReservationService reservationService;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private MovieService movieService;



    @RequestMapping(value = "/session/release")
    public ModelAndView reservationRelease(@RequestParam("session") int sessionId,
                                               @RequestParam("row") List<Integer> rows,
                                               @RequestParam("place") List<Integer> places ) {
        logger.info("here! rows="+rows+" places="+places);
        if (rows.size()!=places.size())
            throw (new BadHttpRequest());
        Session session = sessionService.getById(sessionId);
        logger.info("session="+session);
        if (session==null)
            throw (new BadHttpRequest());

        List<ReservationPlace> placeList = new ArrayList<ReservationPlace>(rows.size());
        for (int i=0; i<rows.size(); i++) {
            placeList.add( new ReservationPlace(rows.get(i)-1, places.get(i)-1) );
        }

        reservationService.release(session, placeList);
        return new ModelAndView("redirect:/session/"+sessionId+"/reserve");
    }

    @RequestMapping(value = "/session/sell")
    public ModelAndView reservationSell(@RequestParam("session") int sessionId,
                                               @RequestParam("row") List<Integer> rows,
                                               @RequestParam("place") List<Integer> places ) {
        logger.info("here! rows="+rows+" places="+places);
        if (rows.size()!=places.size())
            throw (new BadHttpRequest());
        Session session = sessionService.getById(sessionId);
        logger.info("session="+session);
        if (session==null)
            throw (new BadHttpRequest());

        List<ReservationPlace> placeList = new ArrayList<ReservationPlace>(rows.size());
        for (int i=0; i<rows.size(); i++) {
            placeList.add( new ReservationPlace(rows.get(i)-1, places.get(i)-1) );
        }

        reservationService.sell(session, placeList);
        return new ModelAndView("redirect:/session/"+sessionId+"/reserve");
    }

}
