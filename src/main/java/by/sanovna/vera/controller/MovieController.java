package by.sanovna.vera.controller;

import by.sanovna.vera.controller.exceptions.ResourceNotFoundException;
import by.sanovna.vera.model.Movie;
import by.sanovna.vera.service.MovieService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class MovieController {
    private static final Logger logger = Logger.getLogger(MovieController.class);

    @Autowired
    private MovieService movieService;

    @RequestMapping(value = "/movies")
    public ModelAndView movieListPage() {
        logger.info("movies page");
        ModelAndView mav = new ModelAndView("movieList");
        List<Movie> movieList = movieService.getAll();
        logger.info("movies=" + movieList);
        mav.addObject("movies", movieList);
        return mav;
    }

    @RequestMapping(value = "/movie/{id}")
    public ModelAndView moviePage(@PathVariable(value = "id") int movieId) {
        Movie movie = movieService.getById(movieId);
        logger.info("movie="+movie);
        if (movie==null)
            throw (new ResourceNotFoundException());
        movie.sortSessions();
        ModelAndView mav = new ModelAndView("movie");
        logger.info("movie=" + movie);
        mav.addObject("movie", movie);
        return mav;
    }
}
