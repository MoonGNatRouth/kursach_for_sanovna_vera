package by.sanovna.vera.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WelcomeController {
    private static final Logger logger = Logger.getLogger(WelcomeController.class);

    @RequestMapping(value = {"", "/"})
    public ModelAndView welcome() {
        logger.info("this is welcom page");
        ModelAndView mav = new ModelAndView("welcome");
        mav.addObject("message","hello, world!");
        return mav;
    }

    @RequestMapping(value ="/testPage")
    public ModelAndView testPage() {
        logger.info("this is welcom page");
        ModelAndView mav = new ModelAndView("welcome");
        mav.addObject("message","testPage!");
        throw (new RuntimeException("runtime baby!"));
//        return mav;
    }


}
