package by.sanovna.vera.dao;

import by.sanovna.vera.model.Account;

public interface AccountDao {
    Account getByEmail(String email);
    Account getById(long id);

    /**
     * Save giving account and sets to model generated id
     */
    void save(Account account);
    void deleteById(long accountId);
}
