package by.sanovna.vera.dao;

import by.sanovna.vera.model.Reservation;

import java.util.List;

public interface ReservationDao {
    Reservation getById(long reservationId);
    List<Reservation> getByAccountId(long accountId);
    List<Reservation> getBySessionId(long sessionId);
    void deleteById(long reservationId);
    void save(Reservation reservation);
    void sellReservation(long reservationId);
    void deleteAllReservationsForNMinutesBeforeSession(int minutes);
}
