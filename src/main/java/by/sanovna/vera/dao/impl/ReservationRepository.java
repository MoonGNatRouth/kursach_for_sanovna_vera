package by.sanovna.vera.dao.impl;

import by.sanovna.vera.dao.AbstractRepository;
import by.sanovna.vera.dao.AccountDao;
import by.sanovna.vera.dao.DaoException;
import by.sanovna.vera.dao.ReservationDao;
import by.sanovna.vera.dao.SessionDao;
import by.sanovna.vera.model.Reservation;
import by.sanovna.vera.model.ReservationPlace;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

@Repository
public class ReservationRepository extends AbstractRepository implements ReservationDao {
    private final static Logger logger = Logger.getLogger(ReservationRepository.class);

    private static final String SQL_SELECT_RESERVATION_BY_ID = "select session_id, account_id,sold from reservation where id=?";
    private static final String SQL_SELECT_RESERVATION_BY_ACC_ID = "select id,session_id,sold from reservation where account_id=?";
    private static final String SQL_SELECT_RESERVATION_BY_SESSION_ID = "select id, account_id,sold from reservation where session_id=?";
    private static final String SQL_SELECT_RESERVATION_PLACES_BY_ID = "select row, place from reservation_places where reservation_id=?";
    private static final String SQL_DELETE_RESERVATION_BY_ID = "delete from reservation where id=?";
    private static final String SQL_INSERT_INTO_RESERVATION = "insert into reservation (session_id, account_id, sold) values (?,?,?)";
    private static final String SQL_INSERT_INTO_RESERVATION_PLACES = "insert into reservation_places (reservation_id, row, place) values (?,?,?)";
    private static final String SQL_UPDATE_RESERVATION_SET_SOLD_BY_ID = "update reservation set sold=TRUE where id=?";
    private static final String SQL_DELETE_ALL_RESERVATION_FOR_N_MINUTES_BEFORE_SESSION = "delete from reservation where sold=0 and session_id in (select id from session where time_to_sec(TIMEDIFF(date,NOW()))/60 < ?)";

    @Autowired
    private AccountDao accountDao;
    @Autowired
    private SessionDao sessionDao;

    @Override
    public Reservation getById(long reservationId) {
        Reservation reservation=null;
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_SELECT_RESERVATION_BY_ID);
            ps.setLong(1, reservationId);
            logger.info("sql: " + ps.toString());
            rs = ps.executeQuery();
            if (rs.next()) {
                reservation = new Reservation();
                reservation.setId(reservationId);
                reservation.setSold(rs.getBoolean("sold"));
                long sessionId = rs.getLong("session_id");
                reservation.setSession(sessionDao.getById(sessionId));
                long accountId = rs.getLong("account_id");
                reservation.setAccount(accountDao.getById(accountId));
                reservation.setPlaces(getReservationPlacesById(reservationId));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(),e);
            throw (new DaoException("can't get reservation by id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
        return reservation;
    }

    @Override
    public List<Reservation> getByAccountId(long accountId) {
        List<Reservation> result= new LinkedList<Reservation>();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_SELECT_RESERVATION_BY_ACC_ID);
            ps.setLong(1, accountId);
            logger.info("sql: " + ps.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                Reservation reservation = new Reservation();
                reservation.setId(rs.getLong("id"));
                reservation.setSold(rs.getBoolean("sold"));
                long sessionId = rs.getLong("session_id");
                reservation.setSession(sessionDao.getById(sessionId));
                reservation.setAccount(accountDao.getById(accountId));
                reservation.setPlaces(getReservationPlacesById(reservation.getId()));
                result.add(reservation);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(),e);
            throw (new DaoException("can't get reservation by account id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
        return result;
    }

    @Override
    public List<Reservation> getBySessionId(long sessionId) {
        List<Reservation> result= new LinkedList<Reservation>();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_SELECT_RESERVATION_BY_SESSION_ID);
            ps.setLong(1, sessionId);
            logger.info("sql: " + ps.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                Reservation reservation = new Reservation();
                reservation.setId(rs.getLong("id"));
                reservation.setSold(rs.getBoolean("sold"));
                reservation.setSession(sessionDao.getById(sessionId));
                long accountId = rs.getLong("account_id");
                reservation.setAccount(accountDao.getById(accountId));
                reservation.setPlaces(getReservationPlacesById(reservation.getId()));
                result.add(reservation);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(),e);
            throw (new DaoException("can't get reservation by account id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
        return result;
    }

    private List<ReservationPlace> getReservationPlacesById(long reservationId) {
        List<ReservationPlace> result = new LinkedList<ReservationPlace>();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_SELECT_RESERVATION_PLACES_BY_ID);
            ps.setLong(1, reservationId);
            logger.info("sql: " + ps.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                ReservationPlace rp = new ReservationPlace(rs.getInt("row"), rs.getInt("place"));
                result.add(rp);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(),e);
            throw (new DaoException("can't get reservation places by id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
        return result;
    }

    @Override
    public void deleteById(long reservationId) {
        Connection connection = null;
        PreparedStatement ps =null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_DELETE_RESERVATION_BY_ID);
            ps.setLong(1, reservationId);
            logger.info("sql: " + ps.toString());
            ps.execute();
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw (new DaoException("can't delete reservation by id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
        }
    }

    private void saveReservationPlace(long reservationId, ReservationPlace reservationPlace, Connection connection) throws  SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(SQL_INSERT_INTO_RESERVATION_PLACES);
            ps.setLong(1, reservationId);
            ps.setInt(2, reservationPlace.getRow());
            ps.setInt(3, reservationPlace.getPlace());
            logger.info("sql: " + ps.toString());
            ps.execute();
        } finally {
            if (ps!=null) try { ps.close(); } catch (SQLException e) { logger.warn("can't close prepared statement", e); }
        }
    }

    @Override
    public void save(Reservation reservation) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_INSERT_INTO_RESERVATION, Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1,reservation.getSession().getId());
            if (reservation.getAccount()!=null)
                ps.setLong(2,reservation.getAccount().getId());
            else
                ps.setNull(2, Types.INTEGER);
            ps.setBoolean(3,reservation.isSold());
            logger.info("sql: " + ps.toString());
            ps.execute();

            //get generated id
            rs = ps.getGeneratedKeys();
            if (!rs.next())
                throw (new SQLException("can't retrieve generated key."));
            reservation.setId(rs.getLong(1));

            //save reservation places
            for (ReservationPlace reservationPlace : reservation.getPlaces())
                saveReservationPlace(reservation.getId(), reservationPlace, connection);

            //commit changes
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage(),e);
            try {
                if (connection!=null)
                    connection.rollback();
            } catch (SQLException e1) {
                logger.error("Can't rollback",e1);
            }
            throw (new DaoException("can't save account",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
    }

    @Override
    public void sellReservation(long reservationId) {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_UPDATE_RESERVATION_SET_SOLD_BY_ID);
            ps.setLong(1,reservationId);
            logger.info("sql: " + ps.toString());
            ps.execute();

            //commit changes
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage(),e);
            throw (new DaoException("can't sell reservation",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
        }
    }

    @Override
    public void deleteAllReservationsForNMinutesBeforeSession(int minutes) {
        Connection connection = null;
        PreparedStatement ps =null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_DELETE_ALL_RESERVATION_FOR_N_MINUTES_BEFORE_SESSION);
            ps.setLong(1, minutes);
            logger.info("sql: " + ps.toString());
            ps.execute();
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw (new DaoException("can't delete all reservations for N minuter before session",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
        }
    }
}
