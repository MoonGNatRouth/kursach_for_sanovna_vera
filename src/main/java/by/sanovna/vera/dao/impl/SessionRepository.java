package by.sanovna.vera.dao.impl;

import by.sanovna.vera.dao.AbstractRepository;
import by.sanovna.vera.dao.DaoException;
import by.sanovna.vera.dao.MovieDao;
import by.sanovna.vera.dao.SessionDao;
import by.sanovna.vera.model.Movie;
import by.sanovna.vera.model.Session;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

@Repository
public class SessionRepository extends AbstractRepository implements SessionDao {
    private final static Logger logger = Logger.getLogger(SessionRepository.class);

    private final static String SQL_SELECT_SESSION_BY_ID = "select id, movie_id, date, cost from session where id=?";
    private final static String SQL_SELECT_SESSION_BY_MOVIE_ID = "select id, movie_id, date, cost from session where movie_id=?";
    private final static String SQL_INSERT_SESSION = "insert into session (movie_id, date,cost) values (?,?,?)";
    private final static String SQL_UPDATE_SESSION = "update session set movie_id=?, date=?, cost=? where id=?";
    private final static String SQL_DELETE_SESSION_BY_ID = "delete from session where id=?";

    @Autowired
    private MovieDao movieDao;

    /**
     * find session by Id. Also load session movie and all sessions for this movie
     */
    @Override
    public Session getById(long sessionId) {
        Session session = null;
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_SELECT_SESSION_BY_ID);
            ps.setLong(1, sessionId);
            logger.info("sql: " + ps.toString());
            rs = ps.executeQuery();

            if (rs.next()) {
                long movieId = rs.getLong("movie_id");
                Movie movie = movieDao.getById(movieId);
                for (Session movieSession : movie.getSessions())
                    if (movieSession.getId()==sessionId)
                        session=movieSession;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw (new DaoException("can't find session by id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) { logger.warn("can't close prepared statement", e); }
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
        return session;
    }

    /**
     * find session for this movie. sets movie field for each session to given movie model
     */
    @Override
    public List<Session> getByMovie(Movie movie) {
        List<Session> result = new LinkedList<Session>();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_SELECT_SESSION_BY_MOVIE_ID);
            ps.setLong(1, movie.getId());
            logger.info("sql: " + ps.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                Session session = new Session();
                session.setId(rs.getLong("id"));
                session.setMovie(movie);
                session.setDate(rs.getTimestamp("date"));
                session.setCost(rs.getFloat("cost"));
                result.add(session);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw (new DaoException("can't find session by movie id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) { logger.warn("can't close prepared statement", e); }
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
        return result;
    }

    /**
     * delete session by ID and commit changes
     * @param sessionId session id to delete
     */
    @Override
    public void deleteById(long sessionId) {
        Connection connection = null;
        try {
            connection = dbPool.getConnection();
            deleteById(sessionId, connection);
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw (new DaoException("can't delete session by id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
        }
    }

    /**
     * delete session by Id but doesn't commit changes.
     * @param sessionId  session id to delete
     * @param connection using connection
     * @throws SQLException
     */
    @Override
    public void deleteById(long sessionId, Connection connection) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(SQL_DELETE_SESSION_BY_ID);
            ps.setLong(1, sessionId);
            logger.info("sql: " + ps.toString());
            ps.execute();
        } finally {
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement", e);}
        }
    }

    /**
     * save session in db. session Movie IS NOT SAVED!, using given connection. Doesn't commit changes!
     * @param session session to registerUser
     * @param connection connection to use
     * @throws SQLException
     */
    private void save(Session session, Connection connection) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(SQL_INSERT_SESSION, Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1, session.getMovie().getId());
            ps.setTimestamp(2, new java.sql.Timestamp(session.getDate().getTime()));
            ps.setFloat(3, session.getCost());
            logger.info("sql: " + ps.toString());
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (!rs.next())
                throw (new SQLException("can't retrieve generated key."));
            session.setId(rs.getLong(1));
        } finally {
            if (ps!=null) try { ps.close(); } catch (SQLException e) { logger.warn("can't close prepared statement", e); }
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
    }

    /**
     * updates session with session.id. session Movie IS NOT UPDATED!, using given connection. Doesn't commit changes!
     * @param session session to update. session.id must be not equals 0
     * @param connection connection to use
     * @throws SQLException
     */
    private void update(Session session, Connection connection) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(SQL_UPDATE_SESSION);
            ps.setLong(1, session.getMovie().getId());
            ps.setTimestamp(2, new java.sql.Timestamp(session.getDate().getTime()));
            ps.setFloat(3, session.getCost());
            ps.setLong(4, session.getId());
            logger.info("sql: " + ps.toString());
            ps.execute();
        } finally {
            if (ps!=null) try { ps.close(); } catch (SQLException e) { logger.warn("can't close prepared statement", e); }
        }

    }

    /**
     * save or update given session. if session.id=0 - then create new session else modifies session with this id.
     * Movie mustn't be null and his id mustn't be 0!
     * DOESN'T saveOrUpdate session movie!
     * when session created sets new id to given model
     * @param session session to registerUser
     */
    @Override
    public void saveOrUpdate(Session session) {
        Connection connection= null;
        try {
            connection = dbPool.getConnection();
            saveOrUpdate(session, connection);
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw (new DaoException("can't saveOrUpdate session",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
        }
    }

    /**
     * save or update given session using given connection. if session.id=0 - then create new session else modifies session with this id.
     * when session created sets new id to given model
     * Movie mustn't be null and his id mustn't be 0!
     * DOESN'T saveOrUpdate session movie!
     * Doesn't commit changes!
     * @param session session to registerUser
     */
    @Override
    public void saveOrUpdate(Session session, Connection connection) throws SQLException {
        if (session.getId()==0) {
            //creating new session
            save(session,connection);
        } else {
            //update
            update(session,connection);
        }
    }

}
