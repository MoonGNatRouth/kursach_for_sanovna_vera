package by.sanovna.vera.dao.impl;

import by.sanovna.vera.dao.AbstractRepository;
import by.sanovna.vera.dao.AccountDao;
import by.sanovna.vera.dao.DaoException;
import by.sanovna.vera.model.Account;
import by.sanovna.vera.model.AccountRole;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class AccountRepository extends AbstractRepository implements AccountDao {
    private final static Logger logger = Logger.getLogger(AccountRepository.class);

    private static final String SQL_SELECT_ACCOUNT_BY_EMAIL = "select id, name, password, role from account where email=?";
    private static final String SQL_SELECT_ACCOUNT_BY_ID = "select email,name, password, role from account where id=?";
    private static final String SQL_INSERT_INTO_ACCOUNT = "insert into account (email, name, password, role) values (?,?,?,?)";
    private static final String SQL_DELETE_ACCOUNT_BY_ID = "delete from account where id=?";

    @Override
    public Account getByEmail(String email) {
        Account account=null;
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_SELECT_ACCOUNT_BY_EMAIL);
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                account = new Account();
                account.setId(rs.getLong("id"));
                account.setEmail(email);
                account.setName(rs.getString("name"));
                account.setPassword(rs.getString("password"));
                account.setRole(AccountRole.valueOf(rs.getString("role")));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(),e);
            throw (new DaoException("can't get account by email",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
        return account;
    }

    @Override
    public Account getById(long id) {
        Account account=null;
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_SELECT_ACCOUNT_BY_ID);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                account = new Account();
                account.setId(id);
                account.setEmail(rs.getString("email"));
                account.setName(rs.getString("name"));
                account.setPassword(rs.getString("password"));
                account.setRole(AccountRole.valueOf(rs.getString("role")));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(),e);
            throw (new DaoException("can't get account by id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
        return account;
    }

    @Override
    public void deleteById(long accountId) {
        Connection connection = null;
        PreparedStatement ps =null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_DELETE_ACCOUNT_BY_ID);
            ps.setLong(1, accountId);
            logger.info("sql: " + ps.toString());
            ps.execute();
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw (new DaoException("can't delete account by id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
        }
    }

    /**
     * Save giving account and sets to model generated id
     */
    @Override
    public void save(Account account) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_INSERT_INTO_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, account.getEmail());
            ps.setString(2, account.getName());
            ps.setString(3, account.getPassword());
            ps.setString(4, account.getRole().toString());
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (!rs.next())
                throw (new SQLException("can't retrieve generated key."));
            account.setId(rs.getLong(1));
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage(),e);
            throw (new DaoException("can't save account",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
    }
}
