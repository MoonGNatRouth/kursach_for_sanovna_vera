package by.sanovna.vera.dao.impl;

import by.sanovna.vera.dao.AbstractRepository;
import by.sanovna.vera.dao.DaoException;
import by.sanovna.vera.dao.MovieDao;
import by.sanovna.vera.dao.SessionDao;
import by.sanovna.vera.model.Movie;
import by.sanovna.vera.model.Session;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

@Repository
public class MovieRepository  extends AbstractRepository implements MovieDao {
    private final static Logger logger = Logger.getLogger(MovieRepository.class);

    private final static String SQL_SELECT_ALL_MOVIES = "select id,name,genre,duration,poster_ext from movie";
    private final static String SQL_SELECT_MOVIE_BY_ID = "select id,name,genre,duration,poster_ext from movie where id=?";
    private final static String SQL_INSERT_MOVIE = "insert into movie (name, genre, duration, poster_ext) values (?,?,?,?)";
    private final static String SQL_UPDATE_MOVIE = "update movie set name=?, genre=?, duration=?, poster_ext=? where id=?";
    private final static String SQL_UPDATE_MOVIE_WITHOUT_POSTER = "update movie set name=?, genre=?, duration=? where id=?";
    private final static String SQL_DELETE_MOVIE_BY_ID = "delete from movie where id=?";


    @Autowired
    private SessionDao sessionDao;

    @Override
    public List<Movie> getAll() {
        List<Movie> result = new LinkedList<Movie>();
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            stmt = connection.createStatement();
            logger.info("sql: " + stmt.toString());
            rs = stmt.executeQuery(SQL_SELECT_ALL_MOVIES);
            while (rs.next()) {
                Movie movie = new Movie();
                movie.setId(rs.getLong("id"));
                movie.setName(rs.getString("name"));
                movie.setGenre(rs.getString("genre"));
                movie.setDuration(rs.getInt("duration"));
                movie.setPosterFileExt(rs.getString("poster_ext"));
                movie.setSessions(sessionDao.getByMovie(movie));
                result.add(movie);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(),e);
            throw (new DaoException("can't get all movies",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (stmt!=null) try {stmt.close();} catch (SQLException e) { logger.warn("cant' close statement",e);}
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
        return result;
    }

    /**
     * @param movieId movie id to find
     * @return null if there is no movie with this id
     */
    @Override
    public Movie getById(long movieId) {
        Movie movie=null;
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_SELECT_MOVIE_BY_ID);
            ps.setLong(1, movieId);
            logger.info("sql: " + ps.toString());
            rs = ps.executeQuery();
            if (rs.next()) {
                movie = new Movie();
                movie.setId(rs.getLong("id"));
                movie.setName(rs.getString("name"));
                movie.setGenre(rs.getString("genre"));
                movie.setDuration(rs.getInt("duration"));
                movie.setPosterFileExt(rs.getString("poster_ext"));
                movie.setSessions(sessionDao.getByMovie(movie));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(),e);
            throw (new DaoException("can't get movie by id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
        return movie;
    }

    /**
     * delete movie
     * @param movieId movie id to delete
     */
    @Override
    public void deleteById(long movieId) {
        Connection connection = null;
        PreparedStatement ps =null;
        try {
            connection = dbPool.getConnection();
            ps = connection.prepareStatement(SQL_DELETE_MOVIE_BY_ID);
            ps.setLong(1, movieId);
            logger.info("sql: " + ps.toString());
            ps.execute();
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw (new DaoException("can't delete movie by id",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
        }
    }

    /**
     * registerUser movie in db without sessions, using given connection. Doesn't commit changes!
     * @param movie movie to registerUser
     * @param connection connection to use
     * @throws SQLException
     */
    private void save(Movie movie, Connection connection) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(SQL_INSERT_MOVIE, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,movie.getName());
            ps.setString(2, movie.getGenre());
            ps.setInt(3, movie.getDuration());
            ps.setString(4, movie.getPosterFileExt());
            logger.info("sql: " + ps.toString());
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (!rs.next())
                throw (new SQLException("can't retrieve generated key."));
            movie.setId(rs.getLong(1));
        } finally {
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
            if (rs!=null) try {rs.close();} catch (SQLException e) { logger.warn("cant' close resultSet",e);}
        }
    }

    /**
     * updates movie with movie.id without sessions, using given connection. Doesn't commit changes!
     * @param movie movie to update. movie.id must be not equals 0
     * @param connection connection to use
     * @throws SQLException
     */
    private void update(Movie movie, Connection connection) throws SQLException{
        PreparedStatement ps = null;
        try {
            if (null==movie.getPosterFileExt() || "".equals(movie.getPosterFileExt())) {
                //if poster is not changed
                ps = connection.prepareStatement(SQL_UPDATE_MOVIE_WITHOUT_POSTER);
                ps.setString(1,movie.getName());
                ps.setString(2,movie.getGenre());
                ps.setInt(3, movie.getDuration());
                ps.setLong(4, movie.getId());
            } else {
                //if poster changed
                ps = connection.prepareStatement(SQL_UPDATE_MOVIE);
                ps.setString(1,movie.getName());
                ps.setString(2,movie.getGenre());
                ps.setInt(3, movie.getDuration());
                ps.setString(4, movie.getPosterFileExt());
                ps.setLong(5, movie.getId());
            }
            logger.info("sql : " + ps.toString());
            ps.execute();
        } finally {
            if (ps!=null) try { ps.close(); } catch (SQLException e) {logger.warn("can't close prepared statement",e);}
        }
    }

    /**
     * save or update given movie. if movie.id=0 - then create new movie else modifies movie with this id.
     * Also saves or updates movie sessions.
     * when movie or it's sessions created then sets new id to given model
     * @param movie movie to registerUser
     */
    @Override
    public void saveOrUpdate(Movie movie) {
        Connection connection = null;
        try {
            connection = dbPool.getConnection();
            if (movie.getId()==0) {
                //creating new movie
                save(movie,connection);
            } else {
                //update
                update(movie,connection);
            }

            //saveOrUpdateSessions
            if (movie.getSessions()!=null) {
                //loading existing sessions for this movie
                List<Session> existSessions = sessionDao.getByMovie(movie);
                List<Long> existSessionsIds = new LinkedList<Long>();
                for (Session session : existSessions)
                    existSessionsIds.add(session.getId());

                //createOrUpdate specified sessions
                for (Session session : movie.getSessions()) {
                    session.setMovie(movie);
                    sessionDao.saveOrUpdate(session, connection);
                    //remove saved ids
                    existSessionsIds.remove(session.getId());
                }

                //delete existing sessions, which is not specified
                for (Long id : existSessionsIds) {
                    sessionDao.deleteById(id,connection);
                }
            }

            //commit changes
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            try {
                if (connection!=null)
                    connection.rollback();
            } catch (SQLException e1) {
                logger.error("Can't rollback",e1);
            }
            throw (new DaoException("can't saveOrUpdate movie",e));
        } finally {
            if (connection!=null) dbPool.putConnection(connection);
        }
    }
}
