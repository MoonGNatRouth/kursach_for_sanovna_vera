package by.sanovna.vera.dao;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class DbPool {
    private static Logger logger =Logger.getLogger(DbPool.class);
    private static int WAIT_POLL_TIME = 4;

    private ArrayBlockingQueue<Connection> connections;
    private int poolSize;

    private String dbUrl;
    private String user;
    private String password;

    public DbPool(String driverClassName, String dbUrl, String user, String password, int poolSize) throws ClassNotFoundException, SQLException {
        this.poolSize = poolSize;
        this.dbUrl =dbUrl;
        this.user= user;
        this.password = password;
        connections = new ArrayBlockingQueue<Connection>(poolSize);

        Class.forName(driverClassName);
        for (int i=0; i<poolSize; i++) {
            connections.add(newConnection());
        }
    }

    public Connection getConnection() throws SQLException {
        try {
            Connection connection = connections.poll(WAIT_POLL_TIME, TimeUnit.SECONDS);
            if (connection==null) {
                throw (new SQLException("can't get connection"));
            } else {
                if (connection.isClosed()) {
                    logger.info("closed connection in pool. replace it");
                    connection = newConnection();
                }
                connection.commit();
                return connection;
            }

        } catch (InterruptedException e) {
            throw(new SQLException("can't get connection"));
        }
    }

    private Connection newConnection() throws SQLException{
        Connection connection = DriverManager.getConnection(dbUrl, user, password);
        connection.setAutoCommit(false);
        return connection;
    }

    public void putConnection(Connection connection) {
        connections.add(connection);
    }

    public void closeAllConnections() {
        logger.debug("cleaning DbPool");
        for (Connection connection : connections) {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.warn("can't close connection");
            }
        }
        connections.clear();
    }
}
