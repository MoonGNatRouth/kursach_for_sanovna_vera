package by.sanovna.vera.dao;

import by.sanovna.vera.model.Movie;
import by.sanovna.vera.model.Session;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface SessionDao {
    /**
     * find session by Id. Also load session movie and all sessions for this movie
     */
    Session getById(long sessionId);
    /**
     * find session for this movie. sets movie field for each session to given movie model
     */
    List<Session> getByMovie(Movie movie);

    /**
     * delete session by ID and commit changes
     * @param sessionId session id to delete
     */
    void deleteById(long sessionId);
    /**
     * delete session by Id but doesn't commit changes.
     * @param sessionId  session id to delete
     * @param connection using connection
     * @throws SQLException
     */
    void deleteById(long sessionId, Connection connection) throws SQLException;
    /**
     * save or update given session. if session.id=0 - then create new session else modifies session with this id.
     * Movie mustn't be null and his id mustn't be 0!
     * DOESN'T saveOrUpdate session movie!
     * when session created sets new id to given model
     * @param session session to registerUser
     */
    void saveOrUpdate(Session session);

    /**
     * save or update given session using given connection. if session.id=0 - then create new session else modifies session with this id.
     * Movie mustn't be null and his id mustn't be 0!
     * when session created sets new id to given model
     * DOESN'T saveOrUpdate session movie!
     * Doesn't commit changes!
     * @param session session to registerUser
     * @param connection connection to use
     */
    void saveOrUpdate(Session session, Connection connection) throws SQLException;
}
