package by.sanovna.vera.dao;

import by.sanovna.vera.model.Movie;

import java.util.List;

public interface MovieDao {
    List<Movie> getAll();
    /**
     * save or update given movie. if movie.id=0 - then create new movie else modifies movie with this id.
     * Also saves or updates movie sessions.
     * when movie or it's sessions created then sets new id to given model
     * @param movie movie to registerUser
     */
    void saveOrUpdate(Movie movie);
    Movie getById(long movieId);
    /**
     * delete movie
     * @param movieId movie id to delete
     */
    void deleteById(long movieId);
}
