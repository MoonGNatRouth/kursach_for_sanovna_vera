package by.sanovna.vera.bean;


import by.sanovna.vera.dao.ReservationDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

public class Scheduler {
    private static Logger logger =Logger.getLogger(Scheduler.class);
    @Autowired
    private ReservationDao reservationDao;

    private int minutesBeforeRelease = 30;

    //Every minute
    @Scheduled(fixedDelay = 60000)
    public void releaseReservations() {
        logger.info("release all reservations for "+minutesBeforeRelease+" minutes before sessions.");
        reservationDao.deleteAllReservationsForNMinutesBeforeSession(minutesBeforeRelease);
    }

    public int getMinutesBeforeRelease() {
        return minutesBeforeRelease;
    }
    public void setMinutesBeforeRelease(int minutesBeforeRelease) {
        this.minutesBeforeRelease = minutesBeforeRelease;
    }
}
