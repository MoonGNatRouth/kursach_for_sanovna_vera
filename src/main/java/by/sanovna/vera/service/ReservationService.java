package by.sanovna.vera.service;

import by.sanovna.vera.model.Reservation;
import by.sanovna.vera.model.ReservationPlace;
import by.sanovna.vera.model.Session;

import java.util.List;

public interface ReservationService {
    Reservation getById(long reservationId);
    List<Reservation> getByAccountId(long accountId);
    List<Reservation> getBySessionId(long sessionId);

    List<List<Reservation>> getReservationMatrix(long sessionId);
    void reserve(String email, Session session, List<ReservationPlace> places);
    void release(Session session, List<ReservationPlace> places);
    void release(long resevationId);
    void sell(Session session, List<ReservationPlace> places);
}
