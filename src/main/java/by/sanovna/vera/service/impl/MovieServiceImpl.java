package by.sanovna.vera.service.impl;

import by.sanovna.vera.dao.MovieDao;
import by.sanovna.vera.model.Movie;
import by.sanovna.vera.model.ImageTypes;
import by.sanovna.vera.service.MovieService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {
    private static final Logger logger = Logger.getLogger(MovieServiceImpl.class);

    @Autowired
    private MovieDao movieDao;

    @Autowired
    private ServletContext servletContext;

    @Override
    public List<Movie> getAll() {
        return movieDao.getAll();
    }

    @Override
    public Movie getById(long movieId) {
        return movieDao.getById(movieId);
    }

    @Override
    public void createOrUpdateMovie(Movie movie) {
        createOrUpdateMovie(movie, null);
    }

    @Override
    public void createOrUpdateMovie(Movie movie, MultipartFile poster) {
        logger.info("creating movie :"+movie);

        //if acceptable poster is specified
        if (poster!=null && !poster.isEmpty() && ImageTypes.containsContentType(poster.getContentType())) {
            ImageTypes imageType = ImageTypes.getByContentType(poster.getContentType());
            //setting image file extension
            movie.setPosterFileExt(imageType.extension());
            //registerUser movie. to get it's id.
            movieDao.saveOrUpdate(movie);
            //if it really saves
            if (movie.getId()!=0) {
                //create if not exists resource/img folder
                File imgFolder = new File(servletContext.getRealPath("/resources/img"));
                if (!imgFolder.exists())
                    imgFolder.mkdirs();

                //saving image with name: resource/img/{movie.id}.{image file extension}
                //old file will be replaced if exists
                File destFile = new File(imgFolder.getAbsolutePath() + File.separator + movie.getId() + "." + imageType.extension());
                try {
                    poster.transferTo(destFile);
                    logger.info("file saved : " + destFile.getAbsolutePath());
                } catch (IOException e) {
                    logger.info("can't transfer image to the server.", e);
                }
            } else {
                logger.warn("movie.id != 0. poster will not be saved");
            }
        } else {
            //if there is no poster. just registerUser
            movieDao.saveOrUpdate(movie);
        }
    }

    @Override
    public void deleteMovie(long movieId) {
        Movie movie = movieDao.getById(movieId);
        if (movie!=null) {
            movieDao.deleteById(movieId);
            File imgFile = new File(servletContext.getRealPath("/resources/img/"+movie.getId()+"."+movie.getPosterFileExt()));
            if (imgFile.exists())
                imgFile.delete();
        }
    }
}
