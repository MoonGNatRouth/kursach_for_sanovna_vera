package by.sanovna.vera.service.impl;

import by.sanovna.vera.dao.AccountDao;
import by.sanovna.vera.model.Account;
import by.sanovna.vera.model.AccountRole;
import by.sanovna.vera.service.AccountService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {
    private static final Logger logger = Logger.getLogger(AccountServiceImpl.class);

    @Autowired
    private AccountDao accountDao;

    @Override
    public Account getAccountByEmail(String email) {
        return accountDao.getByEmail(email);
    }

    @Override
    public boolean registerUser(Account account) {
        if (null != getAccountByEmail(account.getEmail())) {
            return false;
        } else {
            account.setRole(AccountRole.ROLE_USER);
            logger.info("save account="+account);
            accountDao.save(account);
            return true;
        }
    }


}
