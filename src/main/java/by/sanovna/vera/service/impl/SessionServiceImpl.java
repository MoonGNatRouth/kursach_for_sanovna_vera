package by.sanovna.vera.service.impl;

import by.sanovna.vera.dao.SessionDao;
import by.sanovna.vera.model.Session;
import by.sanovna.vera.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SessionServiceImpl implements SessionService {
    @Autowired
    private SessionDao sessionDao;

    @Override
    public Session getById(long sessionId) {
        return sessionDao.getById(sessionId);
    }
}
