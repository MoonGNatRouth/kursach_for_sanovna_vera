package by.sanovna.vera.service.impl;

import by.sanovna.vera.controller.exceptions.BadHttpRequest;
import by.sanovna.vera.dao.AccountDao;
import by.sanovna.vera.dao.ReservationDao;
import by.sanovna.vera.dao.SessionDao;
import by.sanovna.vera.model.Account;
import by.sanovna.vera.model.Reservation;
import by.sanovna.vera.model.ReservationPlace;
import by.sanovna.vera.model.Session;
import by.sanovna.vera.service.ReservationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService {
    private static final Logger logger = Logger.getLogger(ReservationServiceImpl.class);

    @Autowired
    private ReservationDao reservationDao;
    @Autowired
    private SessionDao sessionDao;
    @Autowired
    private AccountDao accountDao;

    private static final int ROW_COUNT = 20;
    private static final int PLACE_COUNT = 14;

    @Override
    public Reservation getById(long reservationId) {
        return reservationDao.getById(reservationId);
    }

    @Override
    public List<Reservation> getByAccountId(long accountId) {
        return reservationDao.getByAccountId(accountId);
    }

    @Override
    public List<Reservation> getBySessionId(long sessionId) {
        return reservationDao.getBySessionId(sessionId);
    }

    @Override
    public List<List<Reservation>> getReservationMatrix(long sessionId) {
        List<Reservation> reservations = reservationDao.getBySessionId(sessionId);
        List<List<Reservation>> result = new ArrayList<List<Reservation>>(ROW_COUNT);
        for (int i=0; i<ROW_COUNT; i++) {
            List<Reservation> row = new ArrayList<Reservation>(PLACE_COUNT);
            result.add(row);
            for (int j=0; j<PLACE_COUNT; j++)
                row.add(null);
        }

        for (Reservation reservation : reservations) {
            for (ReservationPlace rp : reservation.getPlaces()) {
                result.get(rp.getRow()).set(rp.getPlace(), reservation);
            }
        }

        logger.info("reservation matrix");
        for (List<Reservation> row : result) {
            logger.info(row);
        }

        return result;
    }

    @Override
    public void reserve(String email, Session session, List<ReservationPlace> places) {
        logger.info("reserve email="+email+" session="+session+" places="+places);
        for (ReservationPlace rp  : places) {
            //it's ugly. I know
            if (rp.getRow()<0 || rp.getRow() >= ROW_COUNT || rp.getPlace()<0 || rp.getPlace() >= PLACE_COUNT)
                throw (new BadHttpRequest());
        }
        Reservation reservation = new Reservation();
        reservation.setSession(session);
        Account account = accountDao.getByEmail(email);
        reservation.setAccount(account);
        reservation.setPlaces(places);

        reservationDao.save(reservation);
    }

    @Override
    public void release(Session session, List<ReservationPlace> places) {
        logger.info("try to release: ses="+session+" plases="+places);
        //get all reservations for this session
        List<Reservation> dbReservations = reservationDao.getBySessionId(session.getId());
        //finding that, which contains given places
        List<Reservation> reservToDelete = new LinkedList<Reservation>();
        for (Reservation reservation : dbReservations) {
            if (isIntersected(reservation.getPlaces(), places))
                reservToDelete.add(reservation);
        }

        //delete filtered reservations
        for (Reservation reservation : reservToDelete) {
            reservationDao.deleteById(reservation.getId());
        }
    }

    private boolean isIntersected(List<ReservationPlace> places1, List<ReservationPlace> places2) {
        for (ReservationPlace place1 : places1) {
            for (ReservationPlace place2 : places2) {
                if (place1.getRow()==place2.getRow() && place1.getPlace()==place2.getPlace())
                    return true;
            }
        }
        return false;
    }

    @Override
    public void sell(Session session, List<ReservationPlace> places) {
        logger.info("try to sell: ses="+session+" plases="+places);
        //get all reservations for this session
        List<Reservation> dbReservations = reservationDao.getBySessionId(session.getId());
        //finding that, which contains given places
        List<Reservation> reservToUpdate = new LinkedList<Reservation>();
        for (Reservation reservation : dbReservations) {
            boolean intersected = false;
            for (Iterator<ReservationPlace> iter = places.iterator(); iter.hasNext();) {
                ReservationPlace rp = iter.next();
                for (ReservationPlace rp2 : reservation.getPlaces())
                    if (rp.getRow()==rp2.getRow() && rp.getPlace()==rp2.getPlace()) {
                        iter.remove();
                        intersected=true;
                    }
            }
            if (intersected)
                reservToUpdate.add(reservation);
        }

        logger.info("sell existing reservations="+reservToUpdate);
        for (Reservation reservation : reservToUpdate) {
            reservationDao.sellReservation(reservation.getId());
        }

        if (places.size()>0) {
            Reservation reservation = new Reservation();
            reservation.setSession(session);
            reservation.setPlaces(places);
            reservation.setSold(true);
            reservation.setAccount(null);
            logger.info("sell new reservation="+reservation);
            reservationDao.save(reservation);
        }
    }

    @Override
    public void release(long resevationId) {
        reservationDao.deleteById(resevationId);
    }
}
