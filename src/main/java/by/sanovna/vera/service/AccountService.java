package by.sanovna.vera.service;

import by.sanovna.vera.model.Account;

public interface AccountService {
    Account getAccountByEmail(String email);

    /**
     * save to db given account
     * @param account account to register
     * @return true if user created. false if account's email is not available.
     */
    boolean registerUser(Account account);
}
