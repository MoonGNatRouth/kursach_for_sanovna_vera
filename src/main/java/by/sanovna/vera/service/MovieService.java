package by.sanovna.vera.service;

import by.sanovna.vera.model.Movie;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MovieService {
    List<Movie> getAll();
    Movie getById(long movieId);
    void createOrUpdateMovie(Movie movie);
    void createOrUpdateMovie(Movie movie, MultipartFile poster);
    void deleteMovie(long movieId);
}
