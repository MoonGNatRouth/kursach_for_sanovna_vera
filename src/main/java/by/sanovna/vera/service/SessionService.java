package by.sanovna.vera.service;

import by.sanovna.vera.model.Session;

public interface SessionService {
    Session getById(long sessionId);
}
