package by.sanovna.vera.model;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Movie {
    private long id;
    @Size(min = 2, max = 255, message = "name length must be between 2 and 255")
    private String name;
    @Valid
    private List<Session> sessions = new LinkedList<Session>();
    @Size(min = 2, max = 100, message = "genre length must be between 2 and 100")
    private String genre;
    @Max(value = 32000, message = "duration must be less then 32000")
    private int duration;
    private String posterFileExt;

    public void sortSessions() {
        if (sessions != null) {
            Collections.sort(sessions, new Comparator<Session>() {
                @Override
                public int compare(Session o1, Session o2) {
                    return o1.getDate().compareTo(o2.getDate());
                }
            });
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getPosterFileExt() {
        return posterFileExt;
    }

    public void setPosterFileExt(String posterFileExt) {
        this.posterFileExt = posterFileExt;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sessions=" + sessions +
                ", genre='" + genre + '\'' +
                ", duration=" + duration +
                ", posterFileExt='" + posterFileExt + '\'' +
                '}';
    }
}
