package by.sanovna.vera.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class Session {
    private long id;
    private Movie movie;

    @NotNull(message = "type some date")
    @DateTimeFormat(pattern = "HH:mm dd.MM.yyyy")
    private Date date;

    @Min(value = 0, message = "must be not negative")
    private float cost;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public Movie getMovie() {
        return movie;
    }
    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    public float getCost() {
        return cost;
    }
    public void setCost(float cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Session{" +
                "id=" + id +
//                ", movie=" + movie.getId() +
                ", date=" + date +
                ", cost=" + cost +
                '}';
    }
}
