package by.sanovna.vera.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Account {
    private long id;

    @NotNull (message = "Please enter your email")
    @Size(min = 4, max = 45, message = "Email size must be between 4 and 45")
    private String email;

    @NotNull (message = "Please enter your name")
    @Size(min = 2, max = 60, message = "Name size must be between 2 and 45")
    private String name;

    @NotNull (message = "Please enter your password")
    @Size(min = 3, max = 20, message = "Password size must be between 3 and 20")
    private String password;

    private AccountRole role;


    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public AccountRole getRole() {
        return role;
    }
    public void setRole(AccountRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}

