package by.sanovna.vera.model;

public enum AccountRole {
    ROLE_ADMIN, ROLE_USER
}
