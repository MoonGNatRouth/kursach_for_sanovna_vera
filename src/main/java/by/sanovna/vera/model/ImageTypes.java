package by.sanovna.vera.model;


public enum ImageTypes {
    JPG("image/jpeg","jpg"),
    PNG("image/png","png");

    private String contentType;
    private String extension;


    ImageTypes(String contentType, String extension) {
        this.contentType = contentType;
        this.extension = extension;
    }

    public static ImageTypes getByContentType(String contentType) {
        for (ImageTypes imageType : values()) {
            if (imageType.contentType.equals(contentType))
                return imageType;
        }
        return null;
    }
    public static boolean containsContentType(String contentType) {
        for (ImageTypes imageType : values()) {
            if (imageType.contentType.equals(contentType))
                return true;
        }
        return false;
    }

    public String contentType() {
        return contentType;
    }
    public String extension() {
        return extension;
    }
}
