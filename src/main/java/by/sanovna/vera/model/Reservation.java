package by.sanovna.vera.model;

import java.util.LinkedList;
import java.util.List;

public class Reservation {
    private long id;
    private Account account;
    private Session session;
    private List<ReservationPlace> places = new LinkedList<ReservationPlace>();
    private boolean sold;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }
    public void setAccount(Account account) {
        this.account = account;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public List<ReservationPlace> getPlaces() {
        return places;
    }
    public void setPlaces(List<ReservationPlace> places) {
        this.places = places;
    }

    public boolean isSold() {
        return sold;
    }
    public void setSold(boolean sold) {
        this.sold = sold;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", account=" + account +
                ", session=" + session +
                ", places=" + places +
                ", sold=" + sold +
                '}';
    }
}
