package by.sanovna.vera.model;

public class ReservationPlace {
    private int row;
    private int place;

    public ReservationPlace(int row, int place) {
        this.row = row;
        this.place = place;
    }

    public int getRow() {
        return row;
    }
    public void setRow(int row) {
        this.row = row;
    }

    public int getPlace() {
        return place;
    }
    public void setPlace(int place) {
        this.place = place;
    }

    @Override
    public String toString() {
        return "ReservationPlace{" +
                "row=" + row +
                ", place=" + place +
                '}';
    }
}
